import Knex from 'knex';

function up(knex: Knex) {
  return knex.schema
    .createTable('users', table => {
      table.string('id').primary();
      table.string('email');
      table.string('passwordHash');
      table.enum('status', ['ACTIVE', 'PASSIVE'])
      table.bigInteger('createdAt').notNullable();
      table.bigInteger('updatedAt').notNullable();
      table.bigInteger('deletedAt')
  })
  .createTable('books', table => {
    table.string('id').primary();
    table.string('ownerId').references('id').inTable('users');
    table.bigInteger('createdAt').notNullable();
    table.bigInteger('updatedAt').notNullable();
    table.bigInteger('deletedAt')
  })
  .createTable('bookauthors', table => {
    table.string('userId').references('id').inTable('users');
    table.string('bookId').references('id').inTable('books');
  });
}

function down(knex: Knex) {
  return knex.schema
    .dropTableIfExists('bookauthors')
    .dropTableIfExists('books')
    .dropTableIfExists('users');
}

export { up, down };
