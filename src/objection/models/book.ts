import uuid from "uuid";
import { Model } from 'objection';
import { User } from "./user";
import { join } from "path";

export class Book extends Model {
  // Table name is the only required property.
  static tableName = 'books';

  // Schema properties
  id: string;
  ownerId: number;
  owner: User;
  authors: User[];
  createdAt: Date;
  updatedAt: Date;
  deletedAt: Date;

  static jsonSchema = {
    type: 'object',
    required: ['ownerId'],

    properties: {
      id: { type: 'string' },
      ownerId: { type: 'string' },
    }
  };

  static relationMappings = () => ({
    owner: {
      relation: Model.BelongsToOneRelation,
      modelClass: join(__dirname, 'user'),
      join: {
        from: 'books.ownerId',
        to: 'users.id'
      }
    },
    authors: {
      relation: Model.ManyToManyRelation,
      modelClass: join(__dirname, 'user'),
      join: {
        from: 'books.id',
        through: {
          from: 'bookauthors.bookId',
          to: 'bookauthors.userId',
        },
        to: 'users.id'
      }
    }
  });

  $beforeInsert() {
    this.id = uuid.v4();
    this.createdAt = new Date();
    this.updatedAt = new Date();
  }

  $beforeUpdate() {
    this.updatedAt = new Date();
  }

  // Convert DB-row -> Model
  $parseDatabaseJson(json: object) {
    json = super.$parseDatabaseJson(json);
    toDate(json, 'createdAt');
    toDate(json, 'updatedAt');
    return json;
  }

  // Convert Model -> DB-row
  $formatDatabaseJson(json: object) {
    json = super.$formatDatabaseJson(json);
    toTime(json, 'createdAt');
    toTime(json, 'updatedAt');
    return json;
  }
}


function toDate(obj: any, fieldName: string): any {
  if (obj != null && typeof obj[fieldName] === 'number') {
    obj[fieldName] = new Date(obj[fieldName]);
  }
  return obj;
}

function toTime(obj: any, fieldName: string): any {
  if (obj != null && obj[fieldName] != null && obj[fieldName].getTime) {
    obj[fieldName] = obj[fieldName].getTime();
  }
  return obj;
}

