import uuid from "uuid";
import { Model } from 'objection';

// Read more:
// https://github.com/Vincit/objection.js/blob/master/examples/express-ts/src/models/Person.ts

export enum UserStatus {
  ACTIVE = "ACTIVE",
  PASSIVE = "PASSIVE"
}

export class User extends Model {
  // Table name is the only required property.
  static tableName = 'users';

  // Schema properties
  id: string;
  email: string;
  passwordHash: string;
  status: UserStatus;
  createdAt: Date;
  updatedAt: Date;
  deletedAt: Date;

  // Optional JSON schema. This is not the database schema! Nothing is generated
  // based on this. This is only used for validation. Whenever a model instance
  // is created it is checked against this schema. http://json-schema.org/.
  static jsonSchema = {
    type: 'object',
    required: ['email', 'passwordHash'],

    properties: {
      id: { type: 'string' },
      email: { type: 'string', minLength: 1, maxLength: 255 },
      passwordHash: { type: 'string', minLength: 1, maxLength: 255 },
    }
  };

  $beforeInsert() {
    this.id = uuid.v4();
    this.createdAt = new Date();
    this.updatedAt = new Date();
  }

  $beforeUpdate() {
    this.updatedAt = new Date();
  }

  // Convert DB-row -> Model
  $parseDatabaseJson(json: object) {
    json = super.$parseDatabaseJson(json);
    toDate(json, 'createdAt');
    toDate(json, 'updatedAt');
    return json;
  }

  // Convert Model -> DB-row
  $formatDatabaseJson(json: object) {
    json = super.$formatDatabaseJson(json);
    toTime(json, 'createdAt');
    toTime(json, 'updatedAt');
    return json;
  }
}


function toDate(obj: any, fieldName: string): any {
  if (obj != null && typeof obj[fieldName] === 'number') {
    obj[fieldName] = new Date(obj[fieldName]);
  }
  return obj;
}

function toTime(obj: any, fieldName: string): any {
  if (obj != null && obj[fieldName] != null && obj[fieldName].getTime) {
    obj[fieldName] = obj[fieldName].getTime();
  }
  return obj;
}

