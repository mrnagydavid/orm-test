import knex from './index';
import { up } from './migrations/123_init';
import { User } from './models/user';
import { Book } from './models/book';

const MODELS: any[] = [User, Book];
const JOIN_SCHEMAS: string[] = ['bookauthors'];
export const SCHEMAS: string[] = [...MODELS.map(model => model.tableName), ...JOIN_SCHEMAS]
const DROP_SCHEMAS = [...SCHEMAS].reverse();

export function clearDB() {
  return reCreateSchemas();
}

export async function dropSchemas() {
  for (const schema of DROP_SCHEMAS) {
    await knex.schema.dropTableIfExists(schema);
  }
}

export async function createSchemas(): Promise<any> {
  await up(knex);
}

export function reCreateSchemas() {
  return dropSchemas()
  .then(() => createSchemas());
}
