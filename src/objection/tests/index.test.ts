import { typedKeys } from "../../utils";
import { clearDB } from "../utils";
import { User, UserStatus } from "../models/user";
import knex from "..";
import { transaction } from "objection";
import { Book } from "../models/book";

describe("Sequelize-typescript tests", () => {
  beforeEach(() => {
    return clearDB();
  });

  afterAll(() => {
    return knex.destroy();
  })

  describe("Simple tests", () => {
    it("should create a User", () => {
      const sample = {
        email: 'a@b.hu',
        passwordHash: 'hashhash',
        status: UserStatus.ACTIVE
      };
      transaction(User.knex(), trx => {
        return (
          User.query(trx)
            .insertGraph(sample)
        );
      })
      .then((user) => {
        typedKeys(sample).forEach(key => {
          expect(user[key]).toEqual(sample[key]);
        });
        expect(user.createdAt).toBeTruthy();
        expect(user.updatedAt).toBeTruthy();
        return User.query().findById(user.id);
      })
      .then((user) => {
        expect(user).toBeTruthy();
        if (typeof user === "undefined") {
          throw new Error();
        }
        typedKeys(sample).forEach(key => {
          expect(user[key]).toEqual(sample[key]);
        });
        expect(user.createdAt).toBeTruthy();
        expect(user.updatedAt).toBeTruthy();
      });
    });
  });

  describe("Eager/lazy loading", () => {
    let user: any;
    let authors: any;
    beforeEach(() => {
      const sample = {
        email: 'a@b.hu',
        passwordHash: 'hashhash',
        status: UserStatus.ACTIVE
      };

      return User.query().insertGraph([sample, sample, sample])
      .then(userPool => {
        user = userPool.pop();
        authors = userPool;
      });

      // Note: on every third run:
      /*
        console.warn node_modules/bluebird/js/release/debuggability.js:873
          Unhandled rejection Error: ER_NO_SUCH_TABLE: Table 'objection.users' doesn't exist
      */
    });

    it("should not eager load related relations", () => {
      const bookSample = {
        owner: user,
        authors
      };

      return Book.query().insertGraph(bookSample)
      .then(book => {
        expect(book.owner).toBe(user);
        return Book.query().findById(book.id);
      })
      .then(retrievedBook => {
        if (!retrievedBook) {
          throw new Error();
        }
        // Authors are not loaded eagerly!
        // There is no column for the ids (e.g. @RelationId in TypeORM)
        // so they are not loaded either.
        expect(retrievedBook.authors).toBeFalsy();

        // Owner id is accessible!
        expect(retrievedBook.ownerId).toEqual(user.id);
        // Owner is not eager loaded!
        expect(retrievedBook.owner).toBeFalsy();
        // It must be loaded separately.
        return User.query().findById(retrievedBook.ownerId);
      })
      .then((retrievedOwner: any) => {
        // Owner is loaded separately!
        expect(retrievedOwner).toBeTruthy();
      });
    });

    it("should be able to load related relations", () => {
      const bookSample = {
        owner: user,
        authors
      };

      return Book.query().insertGraph(bookSample)
      .then(book => {
        expect(book.owner).toBe(user);
        return Book.query().eager('owner').mergeEager('authors').findById(book.id);
      })
      .then(retrievedBook => {
        if (!retrievedBook) {
          throw new Error();
        }
        // Owner id is accessible!
        expect(retrievedBook.ownerId).toEqual(user.id);
        // Owner object is accessible!
        expect(retrievedBook.owner).toBeTruthy();
        // Author objects are accessible!
        expect(retrievedBook.authors).toBeTruthy();
        expect(retrievedBook.authors.length).toEqual(2);
      });
    });
  });
});
