import Knex from 'knex';
const { Model } = require('objection');

// Initialize knex.
const knex = Knex({
  client: 'mysql',
  connection: {
    host: 'localhost',
    port: 3307,
    database: 'objection',
    user: 'ormtest',
    password: 'ormtest-pw'
  },
  migrations: {
    directory: __dirname + "/migrations"
  }
});

// Give the knex object to objection.
Model.knex(knex);

export default knex;
