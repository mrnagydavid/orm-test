import Sequelize from "sequelize";
const sequelize = new Sequelize('sequalize', 'ormtest', 'ormtest-pw', {
  host: 'localhost',
  port: 3307,
  dialect: 'mysql'
});

export default sequelize;