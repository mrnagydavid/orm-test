import Sequelize from "sequelize";
import sequelize from "../index";
import User from "./user";

const Book = sequelize.define('book', 
{
  id: {
    type: Sequelize.UUID,
    defaultValue: Sequelize.UUIDV4,
    primaryKey: true
  },
}, 
{
  timestamps: true,
  createdAt: 'createdAt',
  updatedAt: 'updatedAt',
  deletedAt: 'deletedAt',
  paranoid: true
});

Book.belongsTo(User, {
  foreignKey: 'ownerId',
  as: "Owner"
});

Book.belongsToMany(User, {
  as: 'Authors',
  through: 'bookauthors',
  foreignKey: 'bookId',
  otherKey: 'userId'
});

export default Book;