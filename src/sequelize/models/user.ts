import Sequelize from "sequelize";
import sequelize from "../index";

const User = sequelize.define('user', 
{
  id: {
    type: Sequelize.UUID,
    defaultValue: Sequelize.UUIDV4,
    primaryKey: true
  },
  email: {
    type: Sequelize.STRING,
    allowNull: false,
  },
  passwordHash: {
    type: Sequelize.TEXT,
  },
  status: {
    // http://docs.sequelizejs.com/manual/tutorial/models-definition.html#array-enum-
    // "Its only supported with PostgreSQL."
    type: Sequelize.ENUM('ACTIVE', 'PASSIVE')
  }
}, 
{
  timestamps: true,
  createdAt: 'createdAt',
  updatedAt: 'updatedAt',
  deletedAt: 'deletedAt',
  paranoid: true
});

export default User;