import Sequelize from "sequelize";
import sequelize from "../index";
import User from "./user";

const Bike = sequelize.define('bike', 
{
  id: {
    type: Sequelize.UUID,
    defaultValue: Sequelize.UUIDV4,
    primaryKey: true
  },
  ownerId: {
    type: Sequelize.UUID,
    references: {
      model: User,
      key: "id"
    }
  }
},
{
  timestamps: true,
  createdAt: 'createdAt',
  updatedAt: 'updatedAt',
  deletedAt: 'deletedAt',
  paranoid: true
});

export default Bike;