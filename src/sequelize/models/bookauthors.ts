import Sequelize from "sequelize";
import sequelize from "../index";
import User from "./user";
import Book from "./book";

const BookAuthors = sequelize.define('bookauthors', 
{
  // Error: SequelizeDatabaseError: Field 'id' doesn't have a default value
  // It would have been Sequelize's responsibility
  // to generate a UUID, but it doesn't happen.
  // Probably a simple AUTO_INCREMENT would work.
  // id: {
  //   type: Sequelize.UUID,
  //   defaultValue: Sequelize.UUIDV4,
  //   primaryKey: true
  // },

  userId: {
    type: Sequelize.UUID,
  },

  bookId: {
    type: Sequelize.UUID,
  },
}, {
  indexes: [
    {
      unique: false,
      fields: ['userId']
    },
    {
      unique: false,
      fields: ['bookId']
    }
  ]
});

BookAuthors.belongsTo(User, {
  foreignKey: 'ownerId',
});

BookAuthors.belongsTo(Book, {
  foreignKey: 'bookId',
});

export default BookAuthors;