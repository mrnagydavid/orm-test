import uuid from "uuid";
import { typedKeys } from "../../utils";
import { clearDB } from "../utils";
import User from "../models/user";
import Bike from "../models/bike";
import sequelize from "..";
import Book from "../models/book";

describe("Sequelize tests", () => {
  beforeAll(() => {
    return sequelize.authenticate();
  });

  beforeEach(() => {
    return clearDB();
  });

  afterAll(() => {
    return sequelize.close();
  })

  describe("Simple tests", () => {
    it("should create a User", () => {
      const sample = {
        email: 'a@b.hu',
        passwordHash: 'hashhash',
        status: 'ACTIVE'
      };

      return User.create(sample)
      .then((user: any) => {
        typedKeys(sample).forEach(key => {
          expect(user[key]).toEqual(sample[key]);
        });
        expect(user.createdAt).toBeTruthy();
        expect(user.updatedAt).toBeTruthy();
        return User.findById(user.id);
      })
      .then((user: any) => {
        typedKeys(sample).forEach(key => {
          expect(user[key]).toEqual(sample[key]);
        });
        expect(user.createdAt).toBeTruthy();
        expect(user.updatedAt).toBeTruthy();
      });
    });
  });

  describe("Standalone Foreign Key", () => {
    let user: any;
    beforeEach(() => {
      const sample = {
        email: 'a@b.hu',
        passwordHash: 'hashhash',
        status: 'ACTIVE'
      };
  
      user = User.build(sample);
      return user.save();
    });

    it("is possible to define a FK w/o object association", () => {
      const bikeSample = {
        ownerId: user.id
      };

      const bike = Bike.build(bikeSample) as any;
      return bike.save()
      .then((bike1: any) => {
        expect(bike1.id).toBeTruthy();
        return Bike.findAll({ 
          where: { 
            ownerId: user.id
          }
        });
      })
      .then((bikesRetrieved: any) => {
        if (!bikesRetrieved) {
          throw new Error();
        }
        expect(bikesRetrieved.length).toBe(1);
        const bikeRetrieved = bikesRetrieved[0];
        expect(bikeRetrieved.id).toEqual(bike.id);
      });
    });

    it("defines a constraint", () => {
      const bikeSample = {
        ownerId: uuid.v4()
      };

      const bike = Bike.build(bikeSample) as any;
      return bike.save()
      .then(() => {
        expect(true).toBe(false);
      })
      .catch((err: any) => {
        expect(err.name).toEqual("SequelizeForeignKeyConstraintError");
      });
    });
  });

  describe("Eager/lazy loading", () => {
    let user: any;
    let authors: any;
    beforeEach(() => {
      const sample = {
        email: 'a@b.hu',
        passwordHash: 'hashhash',
        status: 'ACTIVE'
      };
  
      user = User.build(sample);
      authors = [];
      authors.push(User.build(sample));
      authors.push(User.build(sample));
      return Promise.all([
        user.save(),
        authors.map((author: any) => author.save())
      ]);
    });

    it("should not eager load related relations", () => {
      const bookSample = {
        owner: user,
        authors
      };

      const book = Book.build(bookSample) as any;
      return book.setOwner(user)
      .then(() => book.setAuthors(authors))
      .then(() => {
        // Cannot access owner field... !?!
        expect(book.owner).not.toBe(user);
        return book.save()
      })
      .then((book1: any) => {
        expect(book1.id).toBeTruthy();
        return Book.findById(book1.id);
      })
      .then((retrievedBook: any) => {
        if (!retrievedBook) {
          throw new Error();
        }
        // Authors are not loaded eagerly!
        // There is no column for the ids (e.g. @RelationId in TypeORM)
        // so they are not loaded either.
        expect(retrievedBook.authors).toBeFalsy();

        expect(retrievedBook.id).toEqual(book.id);
        // Owner id is accessible!
        expect(retrievedBook.ownerId).toEqual(user.id);
        // Owner is not eager loaded!
        expect(retrievedBook.Owner).toBeFalsy();
        // But it can be loaded on-demand.
        return retrievedBook.getOwner();
      })
      .then((retrievedOwner: any) => {
        // Owner is loaded on demand!
        expect(retrievedOwner).toBeTruthy();
      });
    });

    it("should be able to load related relations", () => {
      const bookSample = {
        owner: user,
        authors
      };

      const book = Book.build(bookSample) as any;
      return book.setOwner(user)
      .then(() => book.setAuthors(authors))
      .then(() => {
        return book.save()
      })
      .then(() => {
        // The query uses LEFT OUTER JOIN.
        return Book.findById(book.id, {
          include: [{
            model: User,
            as: "Owner"
          }]
        });
      })
      .then((retrievedBook: any) => {
        if (!retrievedBook) {
          throw new Error();
        }
        expect(retrievedBook.id).toEqual(book.id);
        // Owner id is accessible!
        expect(retrievedBook.ownerId).toEqual(user.id);
        // Owner object is accessible!
        expect(retrievedBook.Owner).toBeTruthy();
      });
    });
  });
});
