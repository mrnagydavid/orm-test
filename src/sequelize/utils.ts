import User from "./models/user";
import Book from "./models/book";
import Bike from "./models/bike";
import sequelize from ".";
import BookAuthors from "./models/bookauthors";

export const SCHEMAS: any[] = [User, Book, Bike, BookAuthors];

export function clearDB() {
  return reCreateSchemas();
}

export function dropSchemas() {
  return new Promise((resolve, reject) => {
    sequelize.drop()
    .then(() => resolve())
    .catch(err  => reject(err))
  });
}

export async function createSchemas(): Promise<any> {
  for (const schema of SCHEMAS) {
    await schema.sync({ force: true });
  }
}

export function reCreateSchemas() {
  return dropSchemas()
  .then(() => createSchemas());
}