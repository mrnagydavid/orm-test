import { User } from "./models/user";
import { Book } from "./models/book";
import { Book2 } from "./models/book2";

const SCHEMAS = [User, Book, Book2];
const DROP_SCHEMAS = [...SCHEMAS].reverse();

export async function clearDB() {
  for (const schema of DROP_SCHEMAS) {
    await schema.delete({});
  }
}
