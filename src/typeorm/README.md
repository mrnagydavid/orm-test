```typescript
import { Entity, BaseEntity, PrimaryColumn, Column, Generated, Index, OneToOne, JoinColumn, ManyToMany, JoinTable, RelationId } from 'typeorm';
import { User } from './user';

export enum UserStatus {
  ACTIVE = "ACTIVE",
  PASSIVE = "PASSIVE"
}

@Entity()
export class Book extends BaseEntity {
  @PrimaryColumn()
  @Generated("uuid")
  @Index({ unique: true })
  public id: string;

  @ManyToMany(() => User, {
    eager: false
  })
  @JoinTable()
  authors: User[];

  @RelationId((book: Book) => book.authors)
  authorIds: string[];
}
```

It will query the Book, and then query the junction table to fill the @RelationId.

```SQL
  console.log node_modules/typeorm/platform/PlatformTools.js:209
    query: START TRANSACTION

  console.log node_modules/typeorm/platform/PlatformTools.js:209
    query: INSERT INTO `book`(`id`, `ownerId`) VALUES (?, ?) -- PARAMETERS: ["3a2412fe-83d7-4835-9b59-e6fb7ed1d4e4","48ce40f0-5f7d-4092-9d42-e11d115cff62"]

  console.log node_modules/typeorm/platform/PlatformTools.js:209
    query: INSERT INTO `book_authors_user`(`bookId`, `userId`) VALUES (?, ?), (?, ?) -- PARAMETERS: ["3a2412fe-83d7-4835-9b59-e6fb7ed1d4e4","da80f381-1f68-4b27-ae2a-6dd20b60d0a2","3a2412fe-83d7-4835-9b59-e6fb7ed1d4e4","1107e772-8f95-45d6-8b86-a1153a93cb2e"]

  console.log node_modules/typeorm/platform/PlatformTools.js:209
    query: COMMIT

  console.log node_modules/typeorm/platform/PlatformTools.js:209
    query: SELECT `Book`.`id` AS `Book_id`, `Book`.`ownerId` AS `Book_ownerId` FROM `book` `Book` WHERE `Book`.`id` = ? -- PARAMETERS: ["3a2412fe-83d7-4835-9b59-e6fb7ed1d4e4"]

  console.log node_modules/typeorm/platform/PlatformTools.js:209
    query: SELECT `Book_authors_rid`.`bookId` AS `bookId`, `Book_authors_rid`.`userId` AS `userId` FROM `user` `user` INNER JOIN `book_authors_user` `Book_authors_rid` ON (`Book_authors_rid`.`bookId` = ? AND `Book_authors_rid`.`userId` = `user`.`id`) ORDER BY `Book_authors_rid`.`userId` ASC, `Book_authors_rid`.`bookId` ASC -- PARAMETERS: ["3a2412fe-83d7-4835-9b59-e6fb7ed1d4e4"]
```


```typescript
@Entity()
export class Book extends BaseEntity {
  @PrimaryColumn()
  @Generated("uuid")
  @Index({ unique: true })
  public id: string;

  @ManyToMany(() => User, {
    eager: true
  })
  @JoinTable()
  authors: User[];

  @RelationId((book: Book) => book.authors)
  authorIds: string[];
}
```

It will query the Book and join the junction table for the eager loading, and then still query the junction table to fill the @RelationId.

```SQL
  console.log node_modules/typeorm/platform/PlatformTools.js:209
    query: START TRANSACTION

  console.log node_modules/typeorm/platform/PlatformTools.js:209
    query: INSERT INTO `book`(`id`, `ownerId`) VALUES (?, ?) -- PARAMETERS: ["d601cbd7-825a-428e-9a77-52c2bf386cd3","85badc23-e9a7-4745-97be-35391b941f71"]

  console.log node_modules/typeorm/platform/PlatformTools.js:209
    query: INSERT INTO `book_authors_user`(`bookId`, `userId`) VALUES (?, ?), (?, ?) -- PARAMETERS: ["d601cbd7-825a-428e-9a77-52c2bf386cd3","4668e23f-21b2-4c0b-9ac8-ae647e7a96c9","d601cbd7-825a-428e-9a77-52c2bf386cd3","1ee8ef33-a3dc-4cbd-bd09-f0d38af44ed6"]

  console.log node_modules/typeorm/platform/PlatformTools.js:209
    query: COMMIT

  console.log node_modules/typeorm/platform/PlatformTools.js:209
    query: SELECT `Book`.`id` AS `Book_id`, `Book`.`ownerId` AS `Book_ownerId`, `Book_authors`.`id` AS `Book_authors_id`, `Book_authors`.`email` AS `Book_authors_email`, `Book_authors`.`passwordHash` AS `Book_authors_passwordHash`, `Book_authors`.`status` AS `Book_authors_status`, `Book_authors`.`createdAt` AS `Book_authors_createdAt`, `Book_authors`.`updatedAt` AS `Book_authors_updatedAt`, `Book_authors`.`deletedAt` AS `Book_authors_deletedAt` FROM `book` `Book` LEFT JOIN `book_authors_user` `Book_Book_authors` ON `Book_Book_authors`.`bookId`=`Book`.`id` LEFT JOIN `user` `Book_authors` ON `Book_authors`.`id`=`Book_Book_authors`.`userId` WHERE `Book`.`id` = ? -- PARAMETERS: ["d601cbd7-825a-428e-9a77-52c2bf386cd3"]

  console.log node_modules/typeorm/platform/PlatformTools.js:209
    query: SELECT `Book_authors_rid`.`bookId` AS `bookId`, `Book_authors_rid`.`userId` AS `userId` FROM `user` `user` INNER JOIN `book_authors_user` `Book_authors_rid` ON (`Book_authors_rid`.`bookId` = ? AND `Book_authors_rid`.`userId` = `user`.`id`) OR (`Book_authors_rid`.`bookId` = ? AND `Book_authors_rid`.`userId` = `user`.`id`) ORDER BY `Book_authors_rid`.`userId` ASC, `Book_authors_rid`.`bookId` ASC -- PARAMETERS: ["d601cbd7-825a-428e-9a77-52c2bf386cd3","d601cbd7-825a-428e-9a77-52c2bf386cd3"]
```
