import { Entity, BaseEntity, PrimaryColumn, Column, Generated, Index, CreateDateColumn, UpdateDateColumn } from 'typeorm';

export enum UserStatus {
  ACTIVE = "ACTIVE",
  PASSIVE = "PASSIVE"
}

@Entity()
export class User extends BaseEntity {
  @PrimaryColumn()
  @Generated("uuid")
  @Index({ unique: true })
  public id: string;
  
  @Column()
  email: string;

  @Column()
  passwordHash: string;

  @Column({
    type: "enum",
    enum: UserStatus,
    default: UserStatus.ACTIVE
  })
  status: UserStatus;

  @CreateDateColumn({ type: "timestamp" })
  createdAt: Date;

  @UpdateDateColumn({ type: "timestamp" })
  updatedAt: Date;

  @Column({ type: "timestamp", nullable: true })
  deletedAt: Date;
}
