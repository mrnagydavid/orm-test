import { Entity, BaseEntity, PrimaryColumn, Column, Generated, Index, OneToOne, JoinColumn, ManyToMany, JoinTable, RelationId } from 'typeorm';
import { User } from './user';

export enum UserStatus {
  ACTIVE = "ACTIVE",
  PASSIVE = "PASSIVE"
}

@Entity()
export class Book extends BaseEntity {
  @PrimaryColumn()
  @Generated("uuid")
  @Index({ unique: true })
  public id: string;
  
  // It is not necessary to explicitly name the
  // column in JoinColumn. By default a `${property}Id`
  // field gets created.
  @OneToOne(() => User)
  @JoinColumn({
    name: "ownerId"
  })
  owner: User;

  // This is not necessary.
  // It is automatically created by JoinColumn.
  // Having this column explicitly with @Column
  // is a way to get access to the id and let typescript know about it.
  // Note: @RelationId decorator intends to do something similar,
  // but it creates a "virtual" field... see: `authorIds`
  @Column()
  ownerId: string;

  // The junction table gets created automatically.
  // The @JoinTable is mandatory on one side of the relation.
  @ManyToMany(() => User)
  @JoinTable()
  authors: User[];

  // The authors' ids can be read with @RelationId.
  // This gives access to the ids in the junction table
  // within this class, without having to query it by hand.
  // This means, that whenever a Book instance is read from DB,
  // the junction table gets queried too!
  // As per 0.2.7, this happens in a separate query,
  // instead of an inner join.
  // Since authorIds becomes available in this Class,
  // typescript will allow to use `authorIds` field in a
  // query, because it is part of Partial<Book>,
  // which can give the impression to the developer that it can be
  // used in a query.
  // But this field is virtual: no such column exists in books.
  // Hence the query won't work.
  // As per 0.2.7, filling the @RelationId induces a separate query
  // even if the authors @JoinTable is eager loaded! So a @RelationId
  // is ALWAYS an extra query. And one that involves an unnecessary
  // INNER JOIN too, so it is not even optimized.
  @RelationId((book: Book) => book.authors)
  authorIds: string[];
}
