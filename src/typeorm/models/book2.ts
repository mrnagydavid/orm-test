import { Entity, BaseEntity, PrimaryColumn, Generated, Index, OneToOne, JoinColumn, ManyToMany, JoinTable } from 'typeorm';
import { User } from './user';

export enum UserStatus {
  ACTIVE = "ACTIVE",
  PASSIVE = "PASSIVE"
}

@Entity()
export class Book2 extends BaseEntity {
  @PrimaryColumn()
  @Generated("uuid")
  @Index({ unique: true })
  public id: string;

  @OneToOne(() => User)
  @JoinColumn({
    name: "ownerId"
  })
  owner: User;

  // Promise<> tells TypeORM that this relation is
  // to be loaded lazily, i.e. whenever the `authors`
  // is "getted", the Promise resolution and thus the query
  // starts.
  @ManyToMany(() => User)
  @JoinTable()
  authors: Promise<User[]>;
}
