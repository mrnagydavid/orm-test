import { createConnection, getConnectionOptions } from "typeorm";

const pConnection = getConnectionOptions()
.then(connectionOptions => createConnection(connectionOptions)
);

export default pConnection;
