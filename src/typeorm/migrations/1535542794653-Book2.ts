import {MigrationInterface, QueryRunner} from "typeorm";

export class Book21535542794653 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query("CREATE TABLE `book2` (`id` varchar(255) NOT NULL, `ownerId` varchar(255) NULL, UNIQUE INDEX `IDX_a6eb91659475a642d958e0c4ba` (`id`), UNIQUE INDEX `REL_e9f3209da78f7c5b04ca0eaaa0` (`ownerId`), PRIMARY KEY (`id`)) ENGINE=InnoDB");
        await queryRunner.query("CREATE TABLE `book2_authors_user` (`book2Id` varchar(255) NOT NULL, `userId` varchar(255) NOT NULL, PRIMARY KEY (`book2Id`, `userId`)) ENGINE=InnoDB");
        await queryRunner.query("ALTER TABLE `user` CHANGE `deletedAt` `deletedAt` timestamp NULL");
        await queryRunner.query("ALTER TABLE `book2` ADD CONSTRAINT `FK_e9f3209da78f7c5b04ca0eaaa0c` FOREIGN KEY (`ownerId`) REFERENCES `user`(`id`)");
        await queryRunner.query("ALTER TABLE `book2_authors_user` ADD CONSTRAINT `FK_de28726600e30e3c336d2a3068d` FOREIGN KEY (`book2Id`) REFERENCES `book2`(`id`) ON DELETE CASCADE");
        await queryRunner.query("ALTER TABLE `book2_authors_user` ADD CONSTRAINT `FK_0576470862ec705f326f37d7dbe` FOREIGN KEY (`userId`) REFERENCES `user`(`id`) ON DELETE CASCADE");
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query("ALTER TABLE `book2_authors_user` DROP FOREIGN KEY `FK_0576470862ec705f326f37d7dbe`");
        await queryRunner.query("ALTER TABLE `book2_authors_user` DROP FOREIGN KEY `FK_de28726600e30e3c336d2a3068d`");
        await queryRunner.query("ALTER TABLE `book2` DROP FOREIGN KEY `FK_e9f3209da78f7c5b04ca0eaaa0c`");
        await queryRunner.query("ALTER TABLE `user` CHANGE `deletedAt` `deletedAt` timestamp(0) NULL");
        await queryRunner.query("DROP TABLE `book2_authors_user`");
        await queryRunner.query("DROP INDEX `REL_e9f3209da78f7c5b04ca0eaaa0` ON `book2`");
        await queryRunner.query("DROP INDEX `IDX_a6eb91659475a642d958e0c4ba` ON `book2`");
        await queryRunner.query("DROP TABLE `book2`");
    }

}
