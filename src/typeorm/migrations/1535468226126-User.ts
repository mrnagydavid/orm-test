import {MigrationInterface, QueryRunner} from "typeorm";

export class User1535468226126 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query("CREATE TABLE `user` (`id` varchar(255) NOT NULL, `email` varchar(255) NOT NULL, `passwordHash` varchar(255) NOT NULL, `status` enum ('ACTIVE', 'PASSIVE') NOT NULL DEFAULT 'ACTIVE', `createdAt` timestamp(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6), `updatedAt` timestamp(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6), `deletedAt` timestamp NULL, UNIQUE INDEX `IDX_cace4a159ff9f2512dd4237376` (`id`), PRIMARY KEY (`id`)) ENGINE=InnoDB");
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query("DROP INDEX `IDX_cace4a159ff9f2512dd4237376` ON `user`");
        await queryRunner.query("DROP TABLE `user`");
    }

}
