import {MigrationInterface, QueryRunner} from "typeorm";

export class Book1535528212138 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query("CREATE TABLE `book` (`id` varchar(255) NOT NULL, `ownerId` varchar(255) NOT NULL, UNIQUE INDEX `IDX_a3afef72ec8f80e6e5c310b28a` (`id`), UNIQUE INDEX `REL_b90677e3d515d915033134fc5f` (`ownerId`), PRIMARY KEY (`id`)) ENGINE=InnoDB");
        await queryRunner.query("CREATE TABLE `book_authors_user` (`bookId` varchar(255) NOT NULL, `userId` varchar(255) NOT NULL, PRIMARY KEY (`bookId`, `userId`)) ENGINE=InnoDB");
        await queryRunner.query("ALTER TABLE `user` CHANGE `deletedAt` `deletedAt` timestamp NULL");
        await queryRunner.query("ALTER TABLE `book` ADD CONSTRAINT `FK_b90677e3d515d915033134fc5f4` FOREIGN KEY (`ownerId`) REFERENCES `user`(`id`)");
        await queryRunner.query("ALTER TABLE `book_authors_user` ADD CONSTRAINT `FK_7775ae6ef3e1a4e3c1e391e7959` FOREIGN KEY (`bookId`) REFERENCES `book`(`id`) ON DELETE CASCADE");
        await queryRunner.query("ALTER TABLE `book_authors_user` ADD CONSTRAINT `FK_bce11c9e74388ee6e509a8411a7` FOREIGN KEY (`userId`) REFERENCES `user`(`id`) ON DELETE CASCADE");
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query("ALTER TABLE `book_authors_user` DROP FOREIGN KEY `FK_bce11c9e74388ee6e509a8411a7`");
        await queryRunner.query("ALTER TABLE `book_authors_user` DROP FOREIGN KEY `FK_7775ae6ef3e1a4e3c1e391e7959`");
        await queryRunner.query("ALTER TABLE `book` DROP FOREIGN KEY `FK_b90677e3d515d915033134fc5f4`");
        await queryRunner.query("ALTER TABLE `user` CHANGE `deletedAt` `deletedAt` timestamp(0) NULL");
        await queryRunner.query("DROP TABLE `book_authors_user`");
        await queryRunner.query("DROP INDEX `REL_b90677e3d515d915033134fc5f` ON `book`");
        await queryRunner.query("DROP INDEX `IDX_a3afef72ec8f80e6e5c310b28a` ON `book`");
        await queryRunner.query("DROP TABLE `book`");
    }

}
