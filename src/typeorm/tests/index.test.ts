import { clearDB } from "../utils";
import pConnection from "..";
import { User, UserStatus } from "../models/user";
import { Book } from "../models/book";
import { typedKeys } from "../../utils";
import { Book2 } from "../models/book2";

describe("TypeORM tests", () => {
  beforeEach(() => {
    return clearDB();
  });

  beforeAll(() => pConnection);
  afterAll(() => {
    pConnection.then(connection => connection.close());
  })

  describe("Simple tests", () => {
    it("should create a User", () => {
      const user = new User();
      user.email = 'a@b.hu';
      user.passwordHash = 'hashhash';
      user.status = UserStatus.ACTIVE;

      return user.save()
      .then(() => {
        expect(user.createdAt).toBeTruthy();
        expect(user.updatedAt).toBeTruthy();
        return User.findOne({ id: user.id });
      })
      .then((retrievedUser) => {
        expect(retrievedUser).toBeTruthy();
        if (!retrievedUser) {
          throw new Error();
        }
        typedKeys(user).forEach(key => {
          expect(retrievedUser[key]).toEqual(user[key]);
        });
        expect(retrievedUser.createdAt).toBeTruthy();
        expect(retrievedUser.updatedAt).toBeTruthy();
      });
    });
  });

  describe("Eager/lazy loading", () => {
    let user: User | undefined;
    let authors: User[];
    beforeEach(() => {
      const sample = {
        email: 'a@b.hu',
        passwordHash: 'hashhash',
        status: 'ACTIVE'
      };
  
      return Promise.all([1, 2, 3].map(() => {
        const user = new User();
        Object.assign(user, sample);
        return user.save();
      }))
      .then((users) => {
        user = users.pop();
        authors = users;
      })
    });

    it("should not eager load related relations", () => {
      const bookSample = {
        owner: user,
        authors
      };

      const book = new Book();
      Object.assign(book, bookSample);
      return book.save()
      .then(() => {
        expect(book.owner).toBe(user);
        expect(book.id).toBeTruthy();
        return Book.findOne({ id: book.id });
      })
      .then(retrievedBook => {
        if (!retrievedBook) {
          throw new Error();
        }
        // Authors are not loaded eagerly!
        expect(retrievedBook.authors).toBeFalsy();

        // But we have access to the authorIds
        // which get loaded due to @RelationId,
        // and as of 0.2.7
        // they are loaded in a separate query.
        expect(retrievedBook.authorIds.sort()).toEqual(bookSample.authors.map((author) => author.id).sort());

        expect(retrievedBook.id).toEqual(book.id);
        // Owner id is accessible!
        expect(retrievedBook.ownerId).toEqual(bookSample.owner!.id);
        // Owner is not eager loaded!
        expect(retrievedBook.owner).toBeFalsy();
        // But it can be loaded in a separate query.
          return User.findOne({ id: retrievedBook.ownerId });
        })
        .then((retrievedOwner: any) => {
          // Owner is loaded on demand!
          expect(retrievedOwner).toBeTruthy();
        });
    });

    it("should be able to load related relations", () => {
      const bookSample = {
        owner: user,
        authors
      };

      const book = new Book();
      Object.assign(book, bookSample);
      return book.save()
      .then(() => {
        expect(book.owner).toBe(user);
        expect(book.id).toBeTruthy();
        return Book.findOne({ id: book.id }, {
          join: {
            alias: "book",
            leftJoinAndSelect: {
              owner: "book.owner",
              authors: "book.authors"
            }
          }
        });
      })
      .then(retrievedBook => {
        if (!retrievedBook) {
          throw new Error();
        }
        expect(retrievedBook.id).toEqual(book.id);
        // Owner id is accessible!
        expect(retrievedBook.ownerId).toEqual(user!.id);
        // Owner object is accessible!
        expect(retrievedBook.owner).toBeTruthy();
        // Author objects are accessible!
        expect(retrievedBook.authors).toBeTruthy();
        expect(retrievedBook.authors.length).toEqual(2);
      });
    });

    it("should load lazy relations", () => {
      const bookSample = {
        owner: user,
        authors
      };

      const book = new Book2();
      Object.assign(book, bookSample);
      return book.save()
      .then(() => {
        expect(book.owner).toBe(user);
        expect(book.id).toBeTruthy();
        return Book2.findOne({ id: book.id });
      })
      .then(retrievedBook => {
        if (!retrievedBook) {
          throw new Error();
        }
        // Authors is a Promise<User[]>,
        // calling its getter will start the
        // resolution of the Promise.
        return retrievedBook.authors;
      })
      .then(authors => {
        expect(authors).toBeTruthy();
        expect(authors.length).toEqual(2);
        expect(authors.map(author => author.id).sort()).toEqual(bookSample.authors.map(author => author.id).sort());
      });
    });
  });
});