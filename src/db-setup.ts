import setup from "./setup";

setup()
.then(() => console.log("Done."))
.catch(err => console.log("Error", err));