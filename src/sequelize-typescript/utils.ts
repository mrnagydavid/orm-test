import { User } from "./models/user";
import { Book } from "./models/book";
import { Bike } from "./models/bike";
import { Bike2 } from "./models/bike2";
import { BookAuthors } from "./models/bookauthors";


export const SCHEMAS: any[] = [User, Book, Bike, Bike2, BookAuthors];
const DROP_SCHEMAS = [...SCHEMAS].reverse();

export function clearDB() {
  return reCreateSchemas();
}

export async function dropSchemas() {
  for (const schema of DROP_SCHEMAS) {
    await schema.drop();
  }
}

// !!! No idea why this doesn't work !!!
// It is a sequelice-typescript addition to have a .sync() on sequelize
// but it does not create the schemas.
// export function createSchemas() {
//   return new Promise((resolve, reject) => {
//     sequelize.sync({ force: true })
//     .then(() => resolve())
//     .catch(err => reject(err));
//   });
// }

export async function createSchemas(): Promise<any> {
  for (const schema of SCHEMAS) {
    await schema.sync({ force: true });
  }
}

export function reCreateSchemas() {
  return dropSchemas()
  .then(() => createSchemas());
}
