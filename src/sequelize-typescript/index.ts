import { Sequelize } from 'sequelize-typescript';

const sequelize =  new Sequelize({
  host: 'localhost',
  port: 3307,
  dialect: 'mysql',
  database: 'sequalize_type',
  username: 'ormtest',
  password: 'ormtest-pw',
  modelPaths: [__dirname + '/models'],
  modelMatch: (filename, member) => {
    return filename === member.toLowerCase();
  },
});

export default sequelize;