import uuid from "uuid";
import { typedKeys } from "../../utils";
import { clearDB } from "../utils";
import { User } from "../models/user";
import sequelize from "..";
import { Bike } from "../models/bike";
import { Bike2 } from "../models/bike2";
import { Book } from "../models/book";

describe("Sequelize-typescript tests", () => {
  beforeAll(() => {
    return sequelize.authenticate();
  });

  beforeEach(() => {
    return clearDB();
  });

  afterAll(() => {
    return sequelize.close();
  })

  describe("Simple tests", () => {
    it("should create a User", () => {
      const sample = {
        email: 'a@b.hu',
        passwordHash: 'hashhash',
        status: 'ACTIVE'
      };

      const user = User.build(sample);

      return user.save()
      .then((user1) => {
        typedKeys(sample).forEach(key => {
          expect(user1[key]).toEqual(sample[key]);
        });
        expect(user1.createdAt).toBeTruthy();
        expect(user1.updatedAt).toBeTruthy();
        return User.findById(user1.id);
      })
      .then((userRetrieved) => {
        if (!userRetrieved) {
          throw new Error();
        }
        typedKeys(sample).forEach(key => {
          expect(userRetrieved[key]).toEqual(sample[key]);
        });
        expect(userRetrieved.createdAt).toBeTruthy();
        expect(userRetrieved.updatedAt).toBeTruthy();
      });
    });
  });

  describe("Standalone Foreign Key", () => {
    let user: User;
    beforeEach(() => {
      const sample = {
        email: 'a@b.hu',
        passwordHash: 'hashhash',
        status: 'ACTIVE'
      };
  
      user = User.build(sample);
      return user.save();
    });

    describe("With @ForeignKey alone", () => {
      it("@ForeignKey w/o @BelongsTo*/@Has* does nothing", () => {
        const bikeSample = {
          ownerId: user.id
        };
  
        const bike = Bike.build(bikeSample);
        return bike.save()
        .then(bike1 => {
          expect(bike1.id).toBeTruthy();
          return Bike.findAll({ 
            where: { 
              ownerId: user.id
            }
          });
        })
        .then(bikesRetrieved => {
          if (!bikesRetrieved) {
            throw new Error();
          }
          expect(bikesRetrieved.length).toBe(1);
          const bikeRetrieved = bikesRetrieved[0];
          expect(bikeRetrieved.id).toEqual(bike.id);
        });
      });
  
      it("does not define a contraint", () => {
        const bikeSample = {
          ownerId: uuid.v4()
        };
  
        const bike = Bike.build(bikeSample);
        return bike.save()
        .then((bike1) => {
          expect(bike1).toBeTruthy();
        });
        // It should have thrown a constraint error
        // if the Foreign Key were set.
      });
    });

    describe("With @Column options", () => {
      it("is possible to define a FK w/o object association", () => {
        const bikeSample = {
          ownerId: user.id
        };
  
        const bike = Bike2.build(bikeSample) as any;
        return bike.save()
        .then((bike1: any) => {
          expect(bike1.id).toBeTruthy();
          return Bike2.findAll({ 
            where: { 
              ownerId: user.id
            }
          });
        })
        .then((bikesRetrieved: any) => {
          if (!bikesRetrieved) {
            throw new Error();
          }
          expect(bikesRetrieved.length).toBe(1);
          const bikeRetrieved = bikesRetrieved[0];
          expect(bikeRetrieved.id).toEqual(bike.id);
        });
      });

      it("defines a constraint", () => {
        const bikeSample = {
          ownerId: uuid.v4()
        };
  
        const bike = Bike2.build(bikeSample) as any;
        return bike.save()
        .then(() => {
          expect(true).toBe(false);
        })
        .catch((err: any) => {
          expect(err.name).toEqual("SequelizeForeignKeyConstraintError");
        });
      });
    });
  });

  describe("Eager/lazy loading", () => {
    let user: User;
    let authors: User[];
    beforeEach(() => {
      const sample = {
        email: 'a@b.hu',
        passwordHash: 'hashhash',
        status: 'ACTIVE'
      };
  
      user = User.build(sample);
      authors = [];
      authors.push(User.build(sample));
      authors.push(User.build(sample));
      return Promise.all([
        user.save(),
        authors.map(author => author.save())
      ]);
    });

    it("should not eager load related relations", () => {
      const bookSample = {
        owner: user,
        authors
      };

      const book = Book.build(bookSample);
      return book.$set('owner', user)
      .then(() => book.$set('authors', authors))
      .then(() => {
        // Cannot access owner field... !?!
        expect(book.owner).not.toBe(user);
        return book.save()
      })
      .then(book1 => {
        expect(book1.id).toBeTruthy();
        return Book.findById(book1.id);
      })
      .then(retrievedBook => {
        if (!retrievedBook) {
          throw new Error();
        }
        // Authors are not loaded eagerly!
        // There is no column for the ids (e.g. @RelationId in TypeORM)
        // so they are not loaded either.
        expect(retrievedBook.authors).toBeFalsy();

        expect(retrievedBook.id).toEqual(book.id);
        // Owner id is accessible!
        expect(retrievedBook.ownerId).toEqual(user.id);
        // Owner is not eager loaded!
        expect(retrievedBook.owner).toBeFalsy();
        // But it can be loaded on-demand.
        return retrievedBook.$get("owner");
      })
      .then((retrievedOwner: any) => {
        // Owner is loaded on demand!
        expect(retrievedOwner).toBeTruthy();
      });
    });

    it("should be able to load related relations", () => {
      const bookSample = {
        owner: user,
        authors
      };

      const book = Book.build(bookSample);
      return book.$set('owner', user)
      .then(() => book.$set('authors', authors))
      .then(() => {
        return book.save()
      })
      .then(() => {
        // The query uses LEFT OUTER JOIN.
        return Book.findById(book.id, {
          include: [
            {
              model: User,
              as: "owner"
            },
            {
              model: User,
              as: "authors"
            }
          ]
        });
      })
      .then(retrievedBook => {
        if (!retrievedBook) {
          throw new Error();
        }
        expect(retrievedBook.id).toEqual(book.id);
        // Owner id is accessible!
        expect(retrievedBook.ownerId).toEqual(user.id);
        // Owner object is accessible!
        expect(retrievedBook.owner).toBeTruthy();
        // Author objects are accessible!
        expect(retrievedBook.authors).toBeTruthy();
        expect(retrievedBook.authors.length).toEqual(2);
      });
    });
  });
});
