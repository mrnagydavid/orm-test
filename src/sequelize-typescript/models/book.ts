import { Model, Table, IsUUID, PrimaryKey, Column, BelongsTo, BelongsToMany, Default, Sequelize } from "sequelize-typescript";
import { User } from "./user";

@Table
export class Book extends Model<Book> {
  @IsUUID(4)
  @Default(Sequelize.UUIDV4)
  @PrimaryKey
  @Column({
    type: Sequelize.UUID
  })
  id: string;

  @BelongsTo(() => User, 'ownerId')
  owner: User; 

  @Column({
    type: Sequelize.UUID
  })
  ownerId: string;

  @BelongsToMany(() => User, 'BookAuthors', 'userId', 'bookId')
  authors: User[]; 
}
