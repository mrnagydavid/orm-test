import { Model, Table, IsUUID, PrimaryKey, Column, Sequelize, AllowNull, CreatedAt, UpdatedAt, DeletedAt, Default } from "sequelize-typescript";

enum UserDecorStatus {
  ACTIVE = "ACTIVE",
  PASSIVE = "PASSIVE"
}

@Table
export class User extends Model<User> {
  @IsUUID(4)
  @Default(Sequelize.UUIDV4)
  @PrimaryKey
  @Column({
    type: Sequelize.UUID
  })
  id: string;

  @AllowNull(false)
  @Column
  email: string;

  @Column(Sequelize.TEXT)
  passwordHash: string;

  @Column(Sequelize.ENUM(Object.keys(UserDecorStatus)))
  status: UserDecorStatus;

  @CreatedAt
  createdAt: Date;

  @UpdatedAt
  updatedAt: Date;

  @DeletedAt
  deletedAt: Date;
}
