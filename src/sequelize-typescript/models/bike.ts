import { Model, Table, IsUUID, PrimaryKey, Column, Default, Sequelize, ForeignKey } from "sequelize-typescript";
import { User } from "./user";

@Table
export class Bike extends Model<Bike> {
  @IsUUID(4)
  @Default(Sequelize.UUIDV4)
  @PrimaryKey
  @Column({
    type: Sequelize.UUID
  })
  id: string;

  // This doesn't work as intended: the column is created,
  // but no FK is attached to it.
  // A working solution can be found in bike2.ts.
  @ForeignKey(() => User)
  @Column
  ownerId: string;
}
