import { Model, Table, Column, Sequelize } from "sequelize-typescript";

@Table({
  indexes: [
    {
      unique: false,
      fields: ['userId']
    },
    {
      unique: false,
      fields: ['bookId']
    }
  ]
})
export class BookAuthors extends Model<BookAuthors> {
  // Error: SequelizeDatabaseError: Field 'id' doesn't have a default value
  // It would have been Sequelize's responsibility
  // to generate a UUID, but it doesn't happen.
  // Probably a simple AUTO_INCREMENT would work.
  // @IsUUID(4)
  // @Default(Sequelize.UUIDV4)
  // @PrimaryKey
  // @Column({
  //   type: Sequelize.UUID
  // })
  // id: string;

  @Column({
    type: Sequelize.UUID
  })
  userId: string; 

  @Column({
    type: Sequelize.UUID
  })
  bookId: string;
}
