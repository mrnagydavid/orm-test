import { Model, Table, IsUUID, PrimaryKey, Column, Default, Sequelize } from "sequelize-typescript";
import { User } from "./user";

@Table
export class Bike2 extends Model<Bike2> {
  @IsUUID(4)
  @Default(Sequelize.UUIDV4)
  @PrimaryKey
  @Column({
    type: Sequelize.UUID
  })
  id: string;

  @Column({
    type: Sequelize.UUID,
    references: {
      // model: "User", // This is lame
      // model: User, // This does not work
      // model: User.getTableName().toString(), // This does not work
      model: User.name, // This is error-prone if we use custom tablenames
      key: "id"
    }
  })
  ownerId: string;
}
