import { createConnection } from "mysql";


const DBs: string[] = [
  "sequalize",
  "sequalize_type",
  "objection",
  "typeorm"
];


function setup() {
  console.log("[SETUP]", "Initializing databases!");
  const sql = createConnection({
    host: 'localhost',
    port: 3307,
    user: 'root',
    password: 'ormtest-root-pw'
  });

  console.log("[SETUP]", "Creating user!");
  return new Promise((resolve, reject) => {
    sql.query(`CREATE USER IF NOT EXISTS 'ormtest'@'%' IDENTIFIED BY 'ormtest-pw'`, (err) => {
      if (err) {
        reject(err);
      } else {
        resolve();
      }
    });
  })
  .then(() => {
    console.log("[SETUP]", "Creating user COMPLETED!");
    return Promise.all(
      DBs.map(dbname => {
        console.log("[SETUP]", `Initializing database ${dbname}!`);
        return new Promise((resolve, reject) => {
          sql.query(`CREATE DATABASE IF NOT EXISTS ${dbname} character set utf8mb4 collate utf8mb4_unicode_ci`, (err) => {
            if (err) {
              reject(err);
            } else {
              resolve();
            }
          });
        })
        .then(() => {
          console.log("[SETUP]", `Database ${dbname} CREATED!`);
          return new Promise((resolve, reject) => {
            sql.query(`GRANT ALL ON ${dbname}.* TO 'ormtest'@'%'`, (err) => {
              if (err) {
                reject(err);
              } else {
                resolve();
              }
            });
          });
        })
        .then(() => {
          console.log("[SETUP]", `Initializing database ${dbname} COMPLETED!`);
        });
      })
    );
  })
  .then(() => {
    sql.end();
  });
}

export default setup;