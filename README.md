# orm-test

Testing ORM solutions for Javascript/Typescript:
- [Sequelize](https://github.com/sequelize/sequelize)
- [Sequelize-typescript](https://github.com/RobinBuschmann/sequelize-typescript)
- [Objection.js (+knex)](https://vincit.github.io/objection.js)
- [TypeORM](https://github.com/typeorm/typeorm/)

Fires up a MySQL5 client listening on host port 3307.

```
docker-compose run --rm test
```

```
docker-compose up -d mysql
yarn
yarn test
docker-compose down
```

Individual tests:
```
yarn test:sequelize
yarn test:sequelize-typescript
yarn test:objection
yarn test:typeorm
```