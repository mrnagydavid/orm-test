module.exports = {
  testURL: "http://localhost/",
  transform: {
    "^.+\\.ts$": "ts-jest",
  },
  moduleFileExtensions: ["ts", "js", "json", "node"],
  testMatch: ["<rootDir>/src/**/*.test.ts"],
  setupTestFrameworkScriptFile: "<rootDir>/src/test-setup.ts"
};
