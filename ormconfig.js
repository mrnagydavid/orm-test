/*
  If this configfile is rewritten as a *.ts file
  the following error is given by typeorm: MissingDriverError: Wrong driver: "undefined" given.
*/

module.exports = {
  type: "mysql",
  host: "localhost",
  port: 3307,
  username: "ormtest",
  password: "ormtest-pw",
  database: "typeorm",
  entities: ['src/typeorm/models/*.ts'],
  migrations: ['src/typeorm/migrations/*.ts'],
  cli: {
    entitiesDir: 'src/typeorm/models',
    migrationsDir: 'src/typeorm/migrations'
  },
  synchronize: false,
  logging: true
};
