FROM node:8-alpine

RUN mkdir /app
WORKDIR /app
ADD . /app
WORKDIR /app

RUN yarn
